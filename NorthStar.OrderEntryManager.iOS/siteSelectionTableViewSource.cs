﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace NorthStar.OrderEntryManager.iOS
{
	public class siteSelectionTableViewSource : UITableViewSource
	{
		List<string> siteIdsAvailable;

		public siteSelectionTableViewSource(List<string> siteIdsAvailable)
		{
            this.siteIdsAvailable = siteIdsAvailable;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = new UITableViewCell(UITableViewCellStyle.Value1, "");
			cell.TextLabel.Text = siteIdsAvailable[indexPath.Row];
			cell.BackgroundColor = UIColor.White;
			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return siteIdsAvailable.Count;
		}
	}
}