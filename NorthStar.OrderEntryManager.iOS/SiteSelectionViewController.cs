using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using WatchConnectivity;

namespace NorthStar.OrderEntryManager.iOS
{
	public partial class SiteSelectionViewController : UIViewController
	{
		//variables that are either passed to next view controller or used here
		public string accessToken;
		public string siteId;
		public List<string> siteIdsAvailable = new List<string>();
        public List<int> siteIdsIndexChosen = new List<int>();
        public List<string> siteNamesAvailable = new List<string>();
        public List<Tuple<string, string>> sitesAvailable = new List<Tuple<string, string>>();

		public SiteSelectionViewController (IntPtr handle) : base (handle)
		{
		}

        public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
        {
            //checks if atleast one or more sites are selected before moving on to next view controller
			if (siteSelectionTable.IndexPathsForSelectedRows == null || siteSelectionTable.IndexPathsForSelectedRows.Length == 0)
			{
				UIAlertView alert = new UIAlertView();
				alert.Title = "Please choose a site.";
				alert.AddButton("OK");
				alert.AlertViewStyle = UIAlertViewStyle.Default;
				alert.DismissWithClickedButtonIndex(0, true);
				alert.Show();
                return false;
            }
            else
            {
                return true;
            }
        }

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
			//gets the information from this view controller and sends it to the next view controller
			siteIdsIndexChosen = new List<int>();
			base.PrepareForSegue(segue, sender);
			var resultViewController = segue.DestinationViewController as salesSummaryViewController;
			resultViewController.accessToken = this.accessToken;
			resultViewController.siteIdsAvailable = this.siteIdsAvailable;
            resultViewController.siteNamesAvailable = this.siteNamesAvailable;
            foreach (NSIndexPath siteIdIndex in siteSelectionTable.IndexPathsForSelectedRows)
            {
                siteIdsIndexChosen.Add(siteIdIndex.Row);
            }
            resultViewController.siteIdsIndexChosen = this.siteIdsIndexChosen;
		}

        partial void siteSelectionButton(UIButton sender)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
			siteIdsAvailable = new List<string>();
			siteNamesAvailable = new List<string>();
			sitesAvailable.Sort();
			foreach (var site in sitesAvailable)
			{
                if (!siteIdsAvailable.Contains(site.Item2))
                {
                    siteIdsAvailable.Add(site.Item2);
                    siteNamesAvailable.Add(site.Item1);
                }
			}
			siteSelectionTable.Source = new siteSelectionTableViewSource(siteNamesAvailable);
            siteSelectionTable.ReloadData();
        }

        public override void ViewDidLoad()
        {
            //some setup for the UI of this view controller
            base.ViewDidLoad();
            siteSelectionColorLabel.BackgroundColor = UIColor.FromRGB(21, 101, 192);
        }
	}
}
