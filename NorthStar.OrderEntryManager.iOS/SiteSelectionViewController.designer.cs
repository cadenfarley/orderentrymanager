// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace NorthStar.OrderEntryManager.iOS
{
	[Register ("SiteSelectionViewController")]
	partial class SiteSelectionViewController
	{
		[Outlet]
		UIKit.UILabel siteSelectionColorLabel { get; set; }

		[Outlet]
		UIKit.UILabel siteSelectionLabel { get; set; }

		[Outlet]
		UIKit.UITableView siteSelectionTable { get; set; }

		[Action ("siteSelectionButton:")]
		partial void siteSelectionButton (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (siteSelectionLabel != null) {
				siteSelectionLabel.Dispose ();
				siteSelectionLabel = null;
			}

			if (siteSelectionColorLabel != null) {
				siteSelectionColorLabel.Dispose ();
				siteSelectionColorLabel = null;
			}

			if (siteSelectionTable != null) {
				siteSelectionTable.Dispose ();
				siteSelectionTable = null;
			}
		}
	}
}
