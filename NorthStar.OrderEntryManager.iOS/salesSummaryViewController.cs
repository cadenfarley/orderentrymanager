using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using NorthStar.OrderEntryManager.DataService;
using System.Threading.Tasks;
using WatchConnectivity;
using Reachability;

namespace NorthStar.OrderEntryManager.iOS
{
    public partial class salesSummaryViewController : UIViewController
    {
        string dateOptionChosen = "";
        NSDate cachedFromDate;
        NSDate cachedToDate;
        int fromYear;
        int fromMonth;
        int fromDay;
        int toYear;
        int toMonth;
        int toDay;
        NSObject _contentObs;
        LoadingOverlay loadPop;
        public string accessToken;
        public string siteId;
        public List<string> siteIdsAvailable;
        public List<int> siteIdsIndexChosen = new List<int>();
        public List<string> siteNamesAvailable = new List<string>();

        public salesSummaryViewController(IntPtr handle) : base(handle)
        {
            //starts the session between the phone and watch to send messages to each other
            WCSessionManager.SharedManager.StartSession();
        }

        //refresh after every 5 minutes
        async void RefreshWithTimer()
        {
            while (true)
            {
                await Task.Delay(300000);
                pullSalesForPhoneAsync();
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            siteId = siteIdsAvailable[siteIdsIndexChosen[0]];
            _contentObs = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, notification => OnBecameActive()); //adds observer and calls method when app becomes active
            RefreshWithTimer();
            selectDatePicker.Date = (NSDate)DateTime.Now.Date;
            List<string> salesSummaryForPhoneListCategories = new List<string>
            {
                "Gross Sales", "Net Sales", "Unfinished Transactions","Payments", "Guest Count",
                "Comps", "Voids", "Discounts", "Labor Hours", "Labor Sales"
            };
			List<string> salesSummaryForPhoneListNumbers = new List<string>
			{
			    "$$$", "$$$", "$$$", "$$$", "$$$",
			    "$$$", "$$$", "$$$", "$$$", "$$$"
			};
            //sets up some UI properties of this view controller
            toDateButton.ContentMode = UIViewContentMode.ScaleToFill;
            fromDateButton.ContentMode = UIViewContentMode.ScaleToFill;
            refreshButton.ContentMode = UIViewContentMode.ScaleToFill;
            settingsButton.ContentMode = UIViewContentMode.ScaleToFill;
            salesSummaryTable.Source = new SalesSummaryTableViewSource(salesSummaryForPhoneListCategories,salesSummaryForPhoneListNumbers);
			this.NavigationItem.Title = siteNamesAvailable[siteIdsIndexChosen[0]];
            this.AutomaticallyAdjustsScrollViewInsets = false;
            salesSummaryTable.AllowsSelection = false;
            salesColorLabel.BackgroundColor = UIColor.FromRGB(21, 101, 192);
            lastPulledDataLabel.TextColor = UIColor.Black;
            selectDatePicker.MaximumDate = selectDatePicker.Date;
            salesSummaryTable.RowHeight = 65;
            cachedFromDate = selectDatePicker.Date;
            cachedToDate = selectDatePicker.Date;
            //takes date from datepicker and converts it into necessary numbers for label/to pull data
			var partsOfDate = selectDatePicker.Date.ToString().Split('-');
			Int32.TryParse(partsOfDate[0], out fromYear);
			Int32.TryParse(partsOfDate[1], out fromMonth);
			Int32.TryParse(partsOfDate[2].Substring(0, 2), out fromDay);
			Int32.TryParse(partsOfDate[0], out toYear);
			Int32.TryParse(partsOfDate[1], out toMonth);
			Int32.TryParse(partsOfDate[2].Substring(0, 2), out toDay);
            fromDateLabel.Text = fromMonth.ToString() + "/" + fromDay.ToString() + "/" + fromYear.ToString();
            toDateLabel.Text = toMonth.ToString() + "/" + toDay.ToString() + "/" + toYear.ToString();
            WCSessionManager.didPhoneConnect = true;
            pullSalesForPhoneAsync();
        }

        public override void ViewWillAppear(bool animated)
        {
            //pulls data view is about to appear
            base.ViewWillAppear(animated);
            WCSessionManager.SharedManager.MessageUpdated += DidReceiveMessage;
			InvokeOnMainThread(() =>
			{
				pullSalesForWatchAsync();
			});
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        void OnBecameActive()
        {
            //pulls data on became active
                InvokeOnMainThread(delegate
                {
                    pullSalesForPhoneAsync();
                });
        }

        partial void RefreshButton_TouchUpInside(UIButton sender)
        {
            //pulls data when refresh button is pressed
			pullSalesForPhoneAsync();
        }

        partial void FromDateButton_TouchUpInside(UIButton sender)
        {
            //login for pressing the fromdate and changes the label for the from date
			dateOptionChosen = "from";
			toDateLabel.Highlighted = false;
			fromDateLabel.Highlighted = true;
			selectDatePicker.Date = cachedFromDate;

			var partsOfDate = selectDatePicker.Date.ToString().Split('-');
			Int32.TryParse(partsOfDate[0], out fromYear);
			Int32.TryParse(partsOfDate[1], out fromMonth);
			Int32.TryParse(partsOfDate[2].Substring(0, 2), out fromDay);
            fromDateLabel.Text = fromMonth.ToString() + "/" + fromDay.ToString() + "/" + fromYear.ToString();
        }

        partial void ToDateButton_TouchUpInside(UIButton sender)
        {
			//login for pressing the todate and changes the label for the to date
			dateOptionChosen = "to";
			fromDateLabel.Highlighted = false;
			toDateLabel.Highlighted = true;
			selectDatePicker.Date = cachedToDate;

			var partsOfDate = selectDatePicker.Date.ToString().Split('-');
			Int32.TryParse(partsOfDate[0], out toYear);
			Int32.TryParse(partsOfDate[1], out toMonth);
			Int32.TryParse(partsOfDate[2].Substring(0, 2), out toDay);
            toDateLabel.Text = toMonth.ToString() + "/" + toDay.ToString() + "/" + toYear.ToString();
		}

		partial void dateChanged(UIDatePicker sender)
        {
            //changes the from and to date labels when date is changed and saves the date that it was changed too
			if (dateOptionChosen == "from")
			{
				selectDatePicker.Date = selectDatePicker.Date.EarlierDate(cachedToDate);
                var partsOfDate = selectDatePicker.Date.ToString().Split('-');
				Int32.TryParse(partsOfDate[0], out fromYear);
				Int32.TryParse(partsOfDate[1], out fromMonth);
				Int32.TryParse(partsOfDate[2].Substring(0, 2), out fromDay);
				fromDateLabel.Text = fromMonth.ToString() + "/" + fromDay.ToString() + "/" + fromYear.ToString();
				cachedFromDate = selectDatePicker.Date;
			}
			else
			{
				selectDatePicker.Date = selectDatePicker.Date.LaterDate(cachedFromDate);
				var partsOfDate = selectDatePicker.Date.ToString().Split('-');
				Int32.TryParse(partsOfDate[0], out toYear);
				Int32.TryParse(partsOfDate[1], out toMonth);
				Int32.TryParse(partsOfDate[2].Substring(0, 2), out toDay);
				toDateLabel.Text = toMonth.ToString() + "/" + toDay.ToString() + "/" + toYear.ToString();
				cachedToDate = selectDatePicker.Date;
			}
        }

		async Task pullSalesForPhoneAsync()
		{
            //checks to see if there is internet or not. then pulls data for phone
            if (!Reachability.Reachability.IsHostReachable("http://google.com"))
            {
                UIAlertView alert = new UIAlertView();
                alert.Title = "Please make sure you are connected to the internet.";
                alert.AddButton("OK");
                alert.AlertViewStyle = UIAlertViewStyle.Default;
                alert.DismissWithClickedButtonIndex(0, true);
                alert.Show();
            }
            else
            {
                lastPulledDataLabel.Text = "Last refreshed on:\n" + DateTime.Now.Date.ToString().Split(' ')[0] + " " + DateTime.Now.TimeOfDay.ToString().Split('.')[0];
                var partsOfDate2 = selectDatePicker.Date.ToString().Split('-');
                Gather salesSummaryForPhone = new Gather(accessToken, siteId, new DateTime(fromYear, fromMonth, fromDay), new DateTime(toYear, toMonth, toDay));
                var bounds = UIScreen.MainScreen.Bounds;
                loadPop = new LoadingOverlay(bounds);
                View.Add(loadPop);
                await salesSummaryForPhone.GenerateTotalsAsync();

				List<string> salesSummaryForPhoneListCategories = new List<string>
				{
				    "Gross Sales",
				    "Net Sales",
				    "Unfinished Trans.",
				    "Payments",
				    "Guest Count",
				    "Comps",
				    "Voids",
				    "Discounts",
				    "Labor Hours",
				    "Labor Wages"
				};

                List<string> salesSummaryForPhoneListNumbers = new List<string>
                {
                    string.Format("{0:C}", salesSummaryForPhone.totalGrossSales),
                    string.Format("{0:C}", salesSummaryForPhone.totalNetSales),
                    string.Format("{0:C}", salesSummaryForPhone.unfinishedTransactions),
                    string.Format("{0:C}", salesSummaryForPhone.totalPayments),
                    string.Format("{0:G}", salesSummaryForPhone.totalGuestCount),
                    string.Format("{0:C}", salesSummaryForPhone.totalComps),
                    string.Format("{0:C}", salesSummaryForPhone.totalVoids),
                    string.Format("{0:C}", salesSummaryForPhone.totalDiscounts),
                    string.Format("{0:G}", salesSummaryForPhone.totalHours),
                    string.Format("{0:C}", salesSummaryForPhone.totalWages)
                };
                salesSummaryTable.Source = new SalesSummaryTableViewSource(salesSummaryForPhoneListCategories, salesSummaryForPhoneListNumbers);
                salesSummaryTable.ReloadData();
                loadPop.Hide();
            }

        }

		async Task pullSalesForWatchAsync()
		{
            //checks to see if watch/phone are paired and pulls data for watch
            if (WCSessionManager.SharedManager.isReachable())
            {
                int currentYear;
                int currentMonth;
                int currentDay;
                string[] partsOfDate2 = ((NSDate)DateTime.Now.Date).ToString().Split('-');
                Int32.TryParse(partsOfDate2[0], out currentYear);
                Int32.TryParse(partsOfDate2[1], out currentMonth);
                Int32.TryParse(partsOfDate2[2].Substring(0, 2), out currentDay);
                Gather salesSummaryForWatch = new Gather(accessToken, siteId, new DateTime(currentYear, currentMonth, currentDay), new DateTime(currentYear, currentMonth, currentDay));
                await salesSummaryForWatch.GenerateTotalsAsync();
	            WCSessionManager.SharedManager.UpdateApplicationContext(new Dictionary<string, object>() { {"GrossSales", string.Format("Gross Sales\n{0:C}", salesSummaryForWatch.totalGrossSales).PadRight(5)}, { "NetSales", string.Format("Net Sales\n{0:C}", salesSummaryForWatch.totalNetSales).PadRight(5) }, { "UnfinishedTransactions", string.Format("Unfin. Trans.\n{0:C}", salesSummaryForWatch.unfinishedTransactions).PadRight(5) },
	            {"Payments", string.Format("Payments\n{0:C}", salesSummaryForWatch.totalPayments).PadRight(5)}, {"GuestCount", string.Format("Guest Count\n{0:C}", salesSummaryForWatch.totalGuestCount).PadRight(5)}, {"Comps", string.Format("Comps\n{0:C}", salesSummaryForWatch.totalComps).PadRight(5)},
	            {"Voids", string.Format("Voids\n{0:C}", salesSummaryForWatch.totalVoids).PadRight(5)}, {"Discounts", string.Format("Discounts\n{0:C}", salesSummaryForWatch.totalDiscounts).PadRight(5)},
	            {"LaborHours", string.Format("Labor Hours\n{0:C}", salesSummaryForWatch.totalHours).PadRight(5)}, {"LaborSales", string.Format("Labor Sales\n{0:C}", salesSummaryForWatch.totalWages).PadRight(5)}, {"Date", "Today's Sales At:\n" + DateTime.Now.TimeOfDay.ToString().Split('.')[0]}, {"isGettingData", false}});
            }
        }

        public void DidReceiveMessage(WCSession session, Dictionary<string, object> message, WCSessionReplyHandler replyHandler)
        {
            //when watch sends a messeage to the phone, the phone will send back the sales data to the watch
	        InvokeOnMainThread(() =>
	        {
                pullSalesForWatchAsync();
	        });
        }
	}
}