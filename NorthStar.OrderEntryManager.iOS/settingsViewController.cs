using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using WatchConnectivity;

namespace NorthStar.OrderEntryManager.iOS
{
    public partial class settingsViewController : UIViewController
    {

		List<string> siteIdsAvailable = new List<string>();
		List<string> siteNamesAvailable = new List<string>();

        public settingsViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
            this.NavigationItem.Title = "Settings";
		}

        partial void LogoutButton_TouchUpInside(UIButton sender)
        {
        }
    }
}