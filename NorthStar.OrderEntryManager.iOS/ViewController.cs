﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using NorthStar.OrderEntryManager.iOS;
using NorthStar.OrderEntryManager.DataService;
using System.Threading.Tasks;
using NorthStar.OrderEntryManager.iOS.EcmAuthenticationServices1;

namespace WatchConnectivity
{
    public partial class ViewController : UIViewController
    {
        //variables that are either passed to next view controller or used here
        string accessToken;
        string cachedUsername; 
        string cachedPassword; 
        List<string> siteIdsAvailable = new List<string>();
        List<string> siteNamesAvailable = new List<string>();
        List<Tuple<string, string>> sitesAvailable = new List<Tuple<string, string>>();

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            //gets the information from this view controller and sends it to the next view controller
            base.PrepareForSegue(segue, sender);
            var resultViewController = segue.DestinationViewController as SiteSelectionViewController;
            resultViewController.accessToken = this.accessToken;
            resultViewController.sitesAvailable = this.sitesAvailable;
        }

        public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
        {
            //checks if there is internet connection/if the login is correct before moving to next view controller
			using (var client = new NorthStar.OrderEntryManager.iOS.EcmAuthenticationServices1.AuthenticationService())
			{
                if (!Reachability.Reachability.IsHostReachable("http://google.com"))
                {
                    //displays alert when there is no internet
                    UIAlertView alert = new UIAlertView();
                    alert.Title = "Please make sure you are connected to the internet.";
                    alert.AddButton("OK");
                    alert.AlertViewStyle = UIAlertViewStyle.Default;
                    alert.DismissWithClickedButtonIndex(0, true);
                    alert.Show();
                    return false;
                }
                else
                {
                    //authenticates user and returns back if the login was valid, an access token, and their allowed sites
					var appId = "9611E3A8-BB03-49C8-B6B5-28CBF082ACAB";
					var response = client.AuthenticateUser(usernameTextField.Text, passwordTextField.Text, appId);
                    if (response.IsAuthenticated)
                    {
                        accessToken = response.AccessToken;
                        for (int i = 0; i < response.AllowedSites.Length; i++)
                        {
                            sitesAvailable.Add(Tuple.Create(response.AllowedSites[i].SiteName, response.AllowedSites[i].SiteId));
                        }
                        NSUserDefaults.StandardUserDefaults.SetString(cachedUsername,"Username");
                        NSUserDefaults.StandardUserDefaults.Init();
						return true;
                    }
                    else
                    {
                        UIAlertView alert = new UIAlertView();
                        alert.Title = "Login is incorrect. Please try again.";
                        alert.AddButton("OK");
                        alert.AlertViewStyle = UIAlertViewStyle.Default;
                        alert.DismissWithClickedButtonIndex(0, true);
                        alert.Show();
                        return false;
                    }
                }
			}
        }


        public override void ViewDidLoad()
        {
            //sets up most of how the UI looks
            base.ViewDidLoad();
            if (NSUserDefaults.StandardUserDefaults.StringForKey("Username") != null)
            {
                cachedUsername = NSUserDefaults.StandardUserDefaults.StringForKey("Username");
            }
            cachedPassword = "";
            usernameTextField.Placeholder = "Username";
            passwordTextField.Placeholder = "Password";
            passwordTextField.SecureTextEntry = true;
            loginLabel.Text = "Pulsar";
            usernameTextField.Text = cachedUsername;
            UIImageView background = new UIImageView(View.Bounds);
            background.Image = UIImage.FromBundle("LoginPicture.jpeg");
            background.ContentMode = UIViewContentMode.ScaleAspectFill;
            background.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            View.AddSubview(background);
            this.NavigationItem.Title = "Home";
            // Perform any additional setup after loading the view, typically from a nib.

            //hides the keyboard after pressing return
            usernameTextField.ShouldReturn = (textField) => { 
                textField.ResignFirstResponder();
                cachedUsername = usernameTextField.Text;
                return true; };
			passwordTextField.ShouldReturn = (textField) =>
			{
				textField.ResignFirstResponder();
                cachedPassword = passwordTextField.Text;
				return true;
			};

            //hides the keyboard after pressing the view
            var g = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);
                cachedUsername = usernameTextField.Text;
                cachedPassword = passwordTextField.Text;
            });
			g.CancelsTouchesInView = false;
			View.AddGestureRecognizer(g);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            cachedPassword = "";
            passwordTextField.Text = cachedPassword;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();
        }

        partial void LoginButton_TouchUpInside(UIButton sender)
        {
        }

        [Action ("UnwindtoLoginViewController:")]
        public void UnwindToLoginViewController (UIStoryboardSegue segue)
        {}

        partial void usernameTextFieldPressed(UITextField sender)
        {
            usernameTextField.Text = cachedUsername;
        }

        partial void passwordTextFieldPressed(UITextField sender)
        {
            passwordTextField.Text = cachedPassword;
        }
    }
}

