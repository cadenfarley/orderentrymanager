﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;



namespace NorthStar.OrderEntryManager.iOS
{
    public class SalesSummaryTableViewSource : UITableViewSource
    {
        private List<string> salesSummaryCategories;
        private List<string> salesSummaryNumbers;

        public SalesSummaryTableViewSource(List<string> salesSummaryCategories, List<string> salesSummaryNumbers)
        {
            this.salesSummaryCategories = salesSummaryCategories;
            this.salesSummaryNumbers = salesSummaryNumbers;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = new UITableViewCell(UITableViewCellStyle.Value1, "");
            cell.TextLabel.Text = salesSummaryCategories[indexPath.Row];
            cell.DetailTextLabel.Text = salesSummaryNumbers[indexPath.Row];
            cell.DetailTextLabel.TextColor = UIColor.Black;
            cell.BackgroundColor = UIColor.White;
            return cell;
        }


        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return salesSummaryCategories.Count;
        }
    }
}
