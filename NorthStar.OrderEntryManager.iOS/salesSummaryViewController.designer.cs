// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace NorthStar.OrderEntryManager.iOS
{
    [Register ("salesSummaryViewController")]
    partial class salesSummaryViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton fromDateButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel fromDateLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lastPulledDataLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton refreshButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel salesColorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView salesSummaryTable { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker selectDatePicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton settingsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton toDateButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel toDateLabel { get; set; }

        [Action ("dateChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void dateChanged (UIKit.UIDatePicker sender);

        [Action ("FromDateButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void FromDateButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("RefreshButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void RefreshButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("ToDateButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ToDateButton_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (fromDateButton != null) {
                fromDateButton.Dispose ();
                fromDateButton = null;
            }

            if (fromDateLabel != null) {
                fromDateLabel.Dispose ();
                fromDateLabel = null;
            }

            if (lastPulledDataLabel != null) {
                lastPulledDataLabel.Dispose ();
                lastPulledDataLabel = null;
            }

            if (refreshButton != null) {
                refreshButton.Dispose ();
                refreshButton = null;
            }

            if (salesColorLabel != null) {
                salesColorLabel.Dispose ();
                salesColorLabel = null;
            }

            if (salesSummaryTable != null) {
                salesSummaryTable.Dispose ();
                salesSummaryTable = null;
            }

            if (selectDatePicker != null) {
                selectDatePicker.Dispose ();
                selectDatePicker = null;
            }

            if (settingsButton != null) {
                settingsButton.Dispose ();
                settingsButton = null;
            }

            if (toDateButton != null) {
                toDateButton.Dispose ();
                toDateButton = null;
            }

            if (toDateLabel != null) {
                toDateLabel.Dispose ();
                toDateLabel = null;
            }
        }
    }
}