﻿using System.Text;
using Android.App;
using Android.Content;
using Android.Gms.Wearable;
using Android.Gms.Common.Apis;

namespace NorthStar.OrderEntryManager.Wear
{
    [Service(), IntentFilter(new string[] { "com.google.android.gms.wearable.BIND_LISTENER", "com.google.android.gms.wearable.MESSAGE_RECEIVED" })]
    public class ReceieverService : WearableListenerService
    {
        GoogleApiClient mGoogleApiClient;
        ISharedPreferences prefs;
        ISharedPreferencesEditor edit;

        public override void OnCreate() {
            base.OnCreate();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                .AddApi(WearableClass.API)
                .Build();
            mGoogleApiClient.Connect();
            prefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            edit = prefs.Edit();
        }

        public override void OnMessageReceived(IMessageEvent MessageEvent) {
            if (MessageEvent.Path.Equals("/authentication")) {
                edit.PutString("authentication", Encoding.UTF8.GetString(MessageEvent.GetData())).Apply();
            }
            if (MessageEvent.Path.Equals("/token")) {
                edit.PutString("token", Encoding.UTF8.GetString(MessageEvent.GetData())).Apply();
            }
            if (MessageEvent.Path.Equals("/siteName")) {
                edit.PutString("siteName", Encoding.UTF8.GetString(MessageEvent.GetData())).Apply();
            }
            if (MessageEvent.Path.Equals("/siteId")) {
                edit.PutString("siteId", Encoding.UTF8.GetString(MessageEvent.GetData())).Apply();
            }
        }
    }
}