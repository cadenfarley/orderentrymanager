﻿using Android.Views;
using Android.Widget;
using Android.Support.Wearable.Views;

namespace NorthStar.OrderEntryManager.Wear
{
    // ViewHolder caches view references.

    class ListViewHolder : WearableRecyclerView.ViewHolder
    {
        public TextView todayView { get; private set; }
        public TextView siteNameView { get; private set; }
        public TextView salesTypeView { get; private set; }
        public TextView salesNumView { get; private set; }
        public TextView lastUpdatedView { get; private set; }

        public ListViewHolder(View view) : base(view) {
            todayView = view.FindViewById<TextView>(Resource.Id.todayView);
            siteNameView = view.FindViewById<TextView>(Resource.Id.siteNameView);
            salesTypeView = view.FindViewById<TextView>(Resource.Id.salesTypeView);
            salesNumView = view.FindViewById<TextView>(Resource.Id.salesNumView);
            lastUpdatedView = view.FindViewById<TextView>(Resource.Id.lastUpdatedView);
        }
    }
}