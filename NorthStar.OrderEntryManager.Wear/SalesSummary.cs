﻿using System.Collections.Generic;
using NorthStar.OrderEntryManager.DataService;

namespace NorthStar.OrderEntryManager.Wear
{
    // SalesSummary stores all the information to be displayed into lists.
    // It converts the monetary values into a currency format.

    public class SalesSummary
    {
        Gather data;
        public List<string> salesType;
        public List<string> salesNum;

        public SalesSummary(Gather today) {
            data = today;
            salesType = new List<string>();
            salesNum = new List<string>();
        }

        public void GetSalesType() {
            salesType.Add("Gross Sales");
            salesType.Add("Net Sales");
            salesType.Add("Payments");
            salesType.Add("Unfinished Transactions");
            salesType.Add("Comps");
            salesType.Add("Discounts");
            salesType.Add("Voids");
            salesType.Add("Guest Count");
            salesType.Add("Labor Hours");
            salesType.Add("Labor Wages");
        }

        public void GetSalesNum() {
            salesNum.Add(data.totalGrossSales.ToString("C"));
            salesNum.Add(data.totalNetSales.ToString("C"));
            salesNum.Add(data.totalPayments.ToString("C"));
            salesNum.Add(data.unfinishedTransactions.ToString("C"));
            salesNum.Add(data.totalComps.ToString("C"));
            salesNum.Add(data.totalDiscounts.ToString("C"));
            salesNum.Add(data.totalVoids.ToString("C"));
            salesNum.Add(data.totalGuestCount.ToString());
            salesNum.Add(data.totalHours.ToString());
            salesNum.Add(data.totalWages.ToString("C"));
        }
    }
}