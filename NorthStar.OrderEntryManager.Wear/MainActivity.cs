﻿using System;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Support.Wearable.Views;
using NorthStar.OrderEntryManager.DataService;

namespace NorthStar.OrderEntryManager.Wear
{
    [Activity(Label = "Pulsar", MainLauncher = true, Icon = "@drawable/ic_launch_star")]
    public class MainActivity : Activity
    {
        protected override async void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            ConnectivityManager connectManager = (ConnectivityManager)GetSystemService(ConnectivityService);
            ISharedPreferences sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);

            if (!(connectManager.ActiveNetworkInfo == null ? false : connectManager.ActiveNetworkInfo.IsConnected)) {
                SetContentView(Resource.Layout.Disconnected);
                return;
            } else if (sharedPrefs.GetString("authentication", "invalid") == "invalid") {
                SetContentView(Resource.Layout.Login);
            } else {
                SetContentView(Resource.Layout.Load);

                // Retrieve site information to pull data
                string accessToken = sharedPrefs.GetString("token", "");
                string siteName = sharedPrefs.GetString("siteName", "");
                string siteId = sharedPrefs.GetString("siteId", "");

                // Generate sales summary data and convert it into correct format
                Gather currentData = new Gather(accessToken, siteId, DateTime.Today, DateTime.Today);
                SalesSummary salesSum = new SalesSummary(currentData);
                salesSum.GetSalesType();
                await currentData.GenerateTotalsAsync();
                salesSum.GetSalesNum();
                currentData = null;
                string currentTime = DateTime.Now.ToShortTimeString();

                // Fill screen with sales summary data
                SetContentView(Resource.Layout.Main);
                WearableRecyclerView recyclerView = FindViewById<WearableRecyclerView>(Resource.Id.wearableRecyclerView);
                ListAdapter listAdapter = new ListAdapter(salesSum, siteName, currentTime);
                recyclerView.SetAdapter(listAdapter);
            }
        }
    }
}