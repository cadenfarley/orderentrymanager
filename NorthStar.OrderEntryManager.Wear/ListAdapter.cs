﻿using System.Collections.Generic;
using Android.Views;
using Android.Support.V7.Widget;
using Android.Support.Wearable.Views;

namespace NorthStar.OrderEntryManager.Wear
{
    // Adapter accesses the data source and populates the screen with data.

    class ListAdapter : WearableRecyclerView.Adapter
    {
        SalesSummary salesSum;
        List<string> salesType;
        List<string> salesNum;
        string siteName;
        string currentTime;

        public ListAdapter(SalesSummary sales, string name, string time) {
            salesSum = sales;
            salesType = salesSum.salesType;
            salesNum = salesSum.salesNum;
            siteName = name;
            currentTime = time;
        }

        public override int ItemCount {
            get { return salesType.Count; }
        }

        // Fills up the RecyclerView with items
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ListViewHolder viewHolder = holder as ListViewHolder;

            viewHolder.todayView.Text = "Today";
            viewHolder.siteNameView.Text = siteName;
            viewHolder.salesTypeView.Text = salesType[position];
            viewHolder.salesNumView.Text = salesNum[position];
            viewHolder.lastUpdatedView.Text = "Last updated " + currentTime;

            if (position < 1) {
                viewHolder.todayView.SetHeight(70);
                viewHolder.siteNameView.SetHeight(90);
                viewHolder.salesTypeView.SetPadding(0, 15, 0, 0);
                viewHolder.salesNumView.SetPadding(0, 0, 0, 10);
                viewHolder.lastUpdatedView.SetHeight(0);
            } else if (position > 8) {
                viewHolder.todayView.SetHeight(0);
                viewHolder.siteNameView.SetHeight(0);
                viewHolder.salesTypeView.SetPadding(0, 0, 0, 0);
                viewHolder.salesNumView.SetPadding(0, 0, 0, 0);
                viewHolder.lastUpdatedView.SetHeight(70);
            } else {
                viewHolder.todayView.SetHeight(0);
                viewHolder.siteNameView.SetHeight(0);
                viewHolder.salesTypeView.SetPadding(0, 0, 0, 0);
                viewHolder.salesNumView.SetPadding(0, 0, 0, 10);
                viewHolder.lastUpdatedView.SetHeight(0);
            }
        }

        // Inflates the layout of each item in the RecyclerView
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new ListViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Item, null));
        }
    }
}