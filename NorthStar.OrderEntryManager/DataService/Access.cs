﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Services.Client;
using Gops.Enterprise;

namespace NorthStar.OrderEntryManager.DataService
{
    public partial class Access
    {
        private static Uri salesUri = new Uri("http://nsoewebservices.cbsnorthstar.com/reportservice/salesdata.svc");
        private static GOPS salesClient = new GOPS(salesUri);

        private static string accessToken;
        private static Guid siteId;

        private static DateTime startDate;
        private static DateTime endDate;

        public Access(string token, string id, DateTime start, DateTime end) {
            accessToken = token;
            siteId = new Guid(id);
            startDate = start;
            endDate = end;
        }

        public static void StartRequest() {
            salesClient.SendingRequest2 += OnSendingRequest;
        }

        public static void EndRequest() {
            salesClient.SendingRequest2 -= OnSendingRequest;
        }

        private static void OnSendingRequest(object sender, SendingRequest2EventArgs e) {
            e.RequestMessage.SetHeader("Authorization", "AccessToken=" + accessToken);
        }

        public static Task<DataServiceCollection<Check>> GetChecksAsync() {
            var query = salesClient.Checks.Where(c => c.BusinessDate >= startDate &&
                                                      c.BusinessDate <= endDate &&
                                                      c.Site_ObjectId == siteId);
            var dsc = new DataServiceCollection<Check>();
            dsc.LoadAsync(query);

            var tcs = new TaskCompletionSource<DataServiceCollection<Check>>();
            dsc.LoadCompleted += (x, y) => tcs.SetResult(dsc);
            return tcs.Task;
        }

        public static Task<DataServiceCollection<TimeRecord>> GetTimeRecordsAsync() {
            var query = salesClient.TimeRecords.Where(t => t.BusinessDate >= startDate &&
                                                           t.BusinessDate <= endDate &&
                                                           t.Site_ObjectId == siteId);
            var dsc = new DataServiceCollection<TimeRecord>();
            dsc.LoadAsync(query);

            var tcs = new TaskCompletionSource<DataServiceCollection<TimeRecord>>();
            dsc.LoadCompleted += (x, y) => tcs.SetResult(dsc);
            return tcs.Task;
        }
    }
}