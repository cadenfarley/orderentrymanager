﻿//using System.Net.Http;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data;
using System.Collections.Generic;

namespace NorthStar.OrderEntryManager.DataService
{
    public class Gather : Access
    {
        public decimal totalGrossSales = 0;
        public decimal totalNetSales   = 0;
        public decimal totalPayments   = 0;
        public decimal unfinishedTransactions = 0;
        public decimal totalComps      = 0;
        public decimal totalDiscounts  = 0;
        public decimal totalVoids      = 0;
        public int     totalGuestCount = 0;
        public decimal totalHours      = 0;
        public decimal totalWages      = 0;

        public string access;
        public string id;
        public DateTime start;
        DateTime end;
        //public static HttpClient client;

        public Gather(string a, string i, DateTime s, DateTime e) : base(a, i, s, e) {
            access = a;
            id = i;
            start = s;
            end = e;
        }

        public async Task GenerateTotalsAsync() {
            StartRequest();

            var checks = await GetChecksAsync();
            foreach (var c in checks) {
                totalGrossSales += (decimal) c.GrossSales;
                totalNetSales   += (decimal) c.NetSales;
                totalPayments   += (decimal) c.CashCollected
                                 + (decimal) c.CreditSalesCollected
                                 + (decimal) c.GiftCardSalesCollected
                                 + (decimal) c.AlternatePaymentsCollected
                                 + (decimal) c.DepositSalesCollected;
                totalComps      += (decimal) c.Comps;
                totalDiscounts  += (decimal) c.Discounts;
                totalVoids      += (decimal) c.Voids;
                totalGuestCount += c.NumberInParty;
                
            }
            unfinishedTransactions = totalNetSales - totalPayments;

            var timeRecords = await GetTimeRecordsAsync();
            foreach (var t in timeRecords) {
                totalHours += (decimal) t.RegularHours
                            + (decimal) t.OvertimeHours
                            + (decimal) t.DoubletimeHours;
                totalWages += (decimal) t.RegularWages
                            + (decimal) t.OvertimeWages
                            + (decimal) t.DoubletimeWages;
            }
            EndRequest();
        }
    }
}