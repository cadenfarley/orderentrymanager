﻿using System;
using WatchKit;
using Foundation;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using NS.OEManager.WatchOSExtension;
using WatchConnectivity;

namespace WatchConnectivity.WatchOSExtension
{
    public partial class InterfaceController : WKInterfaceController, IWCSessionDelegate
    {

		List<string> rows = new List<string>
			{
                "Gross Sales", "Net Sales", "Unfinished Trans.", "Payments", "Guest Count",
                "Comps", "Voids", "Discounts", "Labor Hours", "Labor Sales", "Date", "isGettingData"
			};

        protected InterfaceController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
            WCSessionManager.SharedManager.StartSession();
        }

        public override void Awake(NSObject context)
        {
            base.Awake(context);
            // Configure interface objects here.
            Console.WriteLine("{0} awake with context", this);
        }

        public override void WillActivate()
        {
            base.WillActivate();
            // This method is called when the watch view controller is about to be visible to the user.
            Console.WriteLine("{0} will activate", this);
			WCSessionManager.SharedManager.ApplicationContextUpdated += DidReceiveApplicationContext;
            if (WCSessionManager.SharedManager.isReachable())
            {
                warningLabel.SetHidden(true);
                salesSummaryLabel.SetHidden(false);
                WCSessionManager.SharedManager.SendMessage(new Dictionary<string, object>() { { "TEST", "if we got this, this is good" } });
                MyTable.SetHidden(false);
            }
            else if (!WCSessionManager.SharedManager.isReachable())
            {
                warningLabel.SetText("Please unlock your phone and check that you have internet with the watch and phone connected. Reset both apps after checking.");
                MyTable.SetHidden(true);
                warningLabel.SetHidden(false);
                salesSummaryLabel.SetHidden(true);
            }
        }

		public void DidReceiveApplicationContext(WCSession session, Dictionary<string, object> applicationContext)
		{
            if ((bool)applicationContext["isGettingData"])
            {
                salesSummaryLabel.SetText((string)applicationContext["Date"]);
            }
            else
            {
                InvokeOnMainThread(() =>
                {
                    rows[0] = (string)applicationContext["GrossSales"];
                    rows[1] = (string)applicationContext["NetSales"];
                    rows[2] = (string)applicationContext["UnfinishedTransactions"];
                    rows[3] = (string)applicationContext["Payments"];
                    rows[4] = (string)applicationContext["GuestCount"];
                    rows[5] = (string)applicationContext["Comps"];
                    rows[6] = (string)applicationContext["Voids"];
                    rows[7] = (string)applicationContext["Discounts"];
                    rows[8] = (string)applicationContext["LaborHours"];
                    rows[9] = (string)applicationContext["LaborSales"];
                    salesSummaryLabel.SetText((string)applicationContext["Date"]);
                    rows.RemoveAt(11);
                    rows.RemoveAt(10);
                    LoadTableRows();
                    rows.Add("Date");
                    rows.Add("isGettingData");
                });
            }
		}

        void LoadTableRows()
        {
            MyTable.SetNumberOfRows(rows.Count, "default");
            for (int i = 0; i < rows.Count; i++)
            {
                var elementRow = (RowController)MyTable.GetRowController(i);
                elementRow.MyLabel.SetText(rows[i]);
            }
        }
    }
}

