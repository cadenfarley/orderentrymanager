using Foundation;
using System;
using WatchKit;

namespace NS.OEManager.WatchOSExtension
{
    public partial class LoadingController : WKInterfaceController
    {
        public LoadingController (IntPtr handle) : base (handle)
        {
        }
    }
}