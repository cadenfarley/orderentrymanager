﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using NorthStar.OrderEntryManager.DataService;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "SalesCatagories")]
    public class SalesCatagories : Activity
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;
        Gather gather;

        public string accessToken;
        public string siteId;
        public DateTime startDate;
        public DateTime endDate;

        public HttpClient client;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Helps store and retrieve information by key
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();

            // Set the selected theme
            switch (sharedPrefs.GetString("theme", "light"))
            {
                case "light":
                    SetTheme(Resource.Style.Theme_Light);
                    break;
                case "dark":
                    SetTheme(Resource.Style.Theme_Dark);
                    break;
            }
            SetContentView(Resource.Layout.SalesCatagories);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            accessToken = Intent.GetStringExtra("accessToken");
            siteId = Intent.GetStringExtra("siteId");
            startDate = new DateTime(Intent.GetLongExtra("startDate", 0));
            endDate = new DateTime(Intent.GetLongExtra("endDate", 0));

            gather = new Gather(accessToken, siteId, startDate, endDate);

            String test = "";
            foreach (String s in GetSalesCatagories().Result)
            {
                test += " " + s;
            }
            Toast.MakeText(this, test, ToastLength.Long).Show();
        }
        public async Task<List<String>> GetSalesCatagories()
        {
            client = new HttpClient();
            List<String> SalesCatagories = new List<String>();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            var responseString = await client.GetStringAsync("https://ecm.cbsnorthstar.com/ecm/api/v1/salescategory").ConfigureAwait(false);
            DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(responseString);
            DataTable dataTable = dataSet.Tables["Data"];

            foreach (DataRow row in dataTable.Rows)
            {
                SalesCatagories.Add(row["Name"].ToString());
            }
            return SalesCatagories;
        }
    }
}