﻿using Android.App;
using Android.Content;
using Android.OS;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "Pulsar", Theme = "@style/Theme.Splash", MainLauncher = true, Icon = "@drawable/icon")]
    public class Launch : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            ISharedPreferences sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);

            if (sharedPrefs.GetBoolean("initialLaunch", true) == true) {
                StartActivity(typeof(Login));
                Finish();
            } else {
                StartActivity(typeof(SalesSummary));
                Finish();
            }
        }
    }
}