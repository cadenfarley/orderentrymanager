﻿using System;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace NorthStar.OrderEntryManager.Android
{
    class SiteViewHolder : RecyclerView.ViewHolder
    {
        public TextView siteNameView { get; private set; }
        public ImageView siteSeparator { get; private set; }

        public SiteViewHolder(View view, Action<int> listener) : base(view) {
            siteNameView = view.FindViewById<TextView>(Resource.Id.siteNameView);
            siteSeparator = view.FindViewById<ImageView>(Resource.Id.siteSeparator);

            siteNameView.Click += (sender, e) => listener(base.Position);
        }
    }
}