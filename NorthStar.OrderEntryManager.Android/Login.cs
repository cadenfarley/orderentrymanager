﻿using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Widget;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using NorthStar.OrderEntryManager.Android.Authentication;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "Login", Theme = "@style/Theme.Login")]
    public class Login : Activity
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            // Helps store and retrieve information by key
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();
            sharedPrefsEditor.PutBoolean("initialLaunch", true).Apply();

            // Initialize the contents of the screen
            SetContentView(Resource.Layout.Login);
            var un = FindViewById<EditText>(Resource.Id.unView);
            var pw = FindViewById<EditText>(Resource.Id.pwView);
            var login = FindViewById<Button>(Resource.Id.loginButton);

            login.Click += delegate {
                // Check for internet connection
                // App will crash if there is no internet connection and you try to con
                ConnectivityManager connectManager = (ConnectivityManager)GetSystemService(ConnectivityService);
                if (!(connectManager.ActiveNetworkInfo == null ? false : connectManager.ActiveNetworkInfo.IsConnected)) {
                    Toast.MakeText(this, "No internet connection", ToastLength.Long).Show();
                    return;
                } else {

                    using (var client = new AuthenticationService()) {
                        // Check if entered username and password is valid
                        var appId = "9611E3A8-BB03-49C8-B6B5-28CBF082ACAB";
                        var account = client.AuthenticateUser(un.Text, pw.Text, appId);

                        if (account.IsAuthenticated) {

                            // Keep track of account information on phone
                            sharedPrefsEditor.PutString("username", un.Text).Apply();
                            sharedPrefsEditor.PutString("password", pw.Text).Apply();
                            sharedPrefsEditor.PutString("appId", appId).Apply();
                            sharedPrefsEditor.PutString("token", account.AccessToken).Apply();
                            sharedPrefsEditor.PutString("siteName", account.AllowedSites[0].SiteName).Apply();
                            sharedPrefsEditor.PutString("siteId", account.AllowedSites[0].SiteId).Apply();

                            // Launch site selector screen
                            Intent intent = new Intent(this, typeof(SiteSelector));
                            intent.AddFlags(ActivityFlags.ClearTask);
                            intent.AddFlags(ActivityFlags.NewTask);
                            StartActivity(intent);
                            Finish();
                        } else {
                            Toast.MakeText(this, "Invalid username or password", ToastLength.Short).Show();
                        }
                    }
                }
            };
        }
    }
}