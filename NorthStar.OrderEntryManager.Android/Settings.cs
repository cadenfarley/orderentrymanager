﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

using NorthStar.OrderEntryManager.Android.Authentication;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "Settings")]
    public class Settings : Activity
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;

        AllowedSite[] sites;

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Helps store and retrieve information by key
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();

            // Set the selected theme
            switch (sharedPrefs.GetString("theme", "light")) {
                case "light":
                    SetTheme(Resource.Style.Theme_Light);
                    break;
                case "dark":
                    SetTheme(Resource.Style.Theme_Dark);
                    break;
            }

            // Initialize the contents of the screen
            SetContentView(Resource.Layout.Settings);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.settingsToolbar);
            RadioButton themeLight = FindViewById<RadioButton>(Resource.Id.lightThemeButton);
            RadioButton themeDark = FindViewById<RadioButton>(Resource.Id.darkThemeButton);
            RadioGroup themeGroup = FindViewById<RadioGroup>(Resource.Id.themeGroup);
            Button logout = FindViewById<Button>(Resource.Id.logout);
            Button selectSite = FindViewById<Button>(Resource.Id.selectSite);

            SetActionBar(toolbar);
            ActionBar.SetDisplayHomeAsUpEnabled(true);

            // Keep the selected theme checked
            switch (sharedPrefs.GetString("theme", "light")) {
                case "light":
                    themeGroup.Check(Resource.Id.lightThemeButton);
                    break;
                case "dark":
                    themeGroup.Check(Resource.Id.darkThemeButton);
                    break;
            }


            // Logout click event
            logout.Click += delegate {
                var intent = new Intent(this, typeof(Login));
                intent.AddFlags(ActivityFlags.ClearTask);
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent);
            };

            // Select site click event
            selectSite.Click += delegate {
                var intent = new Intent(this, typeof(SiteSelector));
                intent.AddFlags(ActivityFlags.ClearTask);
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent);
                StartActivity(typeof(SiteSelector));
            };

            // Light theme click event
            themeLight.Click += delegate {
                SetTheme(Resource.Style.Theme_Light);
                sharedPrefsEditor.PutString("theme", "light").Apply();
                Finish();
                var intent = new Intent(this, typeof(SalesSummary));
                intent.AddFlags(ActivityFlags.ClearTask);
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent);
                StartActivity(typeof(Settings));
                Finish();
            };

            // Dark theme click event
            themeDark.Click += delegate {
                SetTheme(Resource.Style.Theme_Dark);
                sharedPrefsEditor.PutString("theme", "dark").Apply();
                Finish();
                var intent = new Intent(this, typeof(SalesSummary));
                intent.AddFlags(ActivityFlags.ClearTask);
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent);
                StartActivity(typeof(Settings));
                Finish();
            };
        }

        // Allows the back button to work
        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case global::Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}