﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using NorthStar.OrderEntryManager.DataService;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "Sales Summary")]
    public class SalesSummary : Activity
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;

        public Gather gather;
        public ProgressDialog progress;
        string accessToken;
        string siteName;
        string siteId;

        public DateTime startDate;
        public DateTime endDate;

        public bool dateShow;
        public Animation spin;

        public Toolbar salesToolbar;
        public Toolbar dateSelectorBar;
        public Button calendarButton;
        public Button refreshButton;
        public Button settingsButton;
        public Button startDateButton;
        public Button endDateButton;

        public TextView grossSales;
        public TextView netSales;
        public TextView payments;
        public TextView unfinishedTransactions;
        public TextView comps;
        public TextView discounts;
        public TextView voids;
        public TextView guestCount;
        public TextView laborHours;
        public TextView laborWages;

        public LinearLayout grossSalesField;

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            // Helps store and retrieve information by key
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();
            sharedPrefsEditor.PutBoolean("initialLaunch", false).Apply();
            accessToken = sharedPrefs.GetString("token", "");

            // Set the selected theme
            switch (sharedPrefs.GetString("theme", "light")) {
                case "light":
                    SetTheme(Resource.Style.Theme_Light);
                    break;
                case "dark":
                    SetTheme(Resource.Style.Theme_Dark);
                    break;
            }

            // Initialize the contents of the screen
            SetContentView(Resource.Layout.SalesSummary);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            startDate = DateTime.Today;
            endDate = DateTime.Today;
            dateShow = false;
            spin = AnimationUtils.LoadAnimation(this, Resource.Animation.spin);

            salesToolbar = FindViewById<Toolbar>(Resource.Id.salesToolbar);
            dateSelectorBar = FindViewById<Toolbar>(Resource.Id.dateSelector);
            calendarButton = FindViewById<Button>(Resource.Id.calendarButton);
            startDateButton = FindViewById<Button>(Resource.Id.startDateButton);
            endDateButton = FindViewById<Button>(Resource.Id.endDateButton);
            refreshButton = FindViewById<Button>(Resource.Id.refreshButton);
            settingsButton = FindViewById<Button>(Resource.Id.settingsButton);

            grossSales = FindViewById<TextView>(Resource.Id.grossSalesView);
            netSales = FindViewById<TextView>(Resource.Id.netSalesView);
            payments = FindViewById<TextView>(Resource.Id.paymentsView);
            unfinishedTransactions = FindViewById<TextView>(Resource.Id.unfinishedTransView);
            comps = FindViewById<TextView>(Resource.Id.compsView);
            discounts = FindViewById<TextView>(Resource.Id.discountsView);
            voids = FindViewById<TextView>(Resource.Id.voidsView);
            guestCount = FindViewById<TextView>(Resource.Id.guestCountView);
            laborHours = FindViewById<TextView>(Resource.Id.laborHoursView);
            laborWages = FindViewById<TextView>(Resource.Id.laborWagesView);

            grossSalesField = FindViewById<LinearLayout>(Resource.Id.grossSalesField);

            // Fill screen with site name and data
            GetData();
            SetActionBar(salesToolbar);
            salesToolbar.Title = siteName + ": " + startDate.ToString("MM/dd") + "-" + endDate.ToString("MM/dd");

            // Refresh click event
            refreshButton.Click += delegate {
                GetData();
                salesToolbar.Title = siteName + ": " + startDate.ToString("MM/dd") + "-" + endDate.ToString("MM/dd");
                refreshButton.StartAnimation(spin);
            };

            // Calendar click event
            calendarButton.Click += delegate {
                if (dateShow == false) {
                    dateShow = true;
                    dateSelectorBar.Visibility = ViewStates.Visible;
                } else {
                    dateShow = false;
                    dateSelectorBar.Visibility = ViewStates.Gone;
                }
            };

            // Start date click event
            startDateButton.Click += delegate {
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time) {
                    if (time > DateTime.Today) {
                        InvalidDateMessage("Selected date is set in the future. Select a valid date.");
                    } else if (time > endDate) {
                        InvalidDateMessage("Selected date is after the end date. Select a valid date.");
                    } else {
                        startDate = time;
                        GetData();
                        startDateButton.Text = "Start: " + startDate.ToShortDateString();
                    }
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);
            };

            // End date click event
            endDateButton.Click += delegate {
                DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time) {
                    if (time > DateTime.Today) {
                        InvalidDateMessage("Selected date is set in the future. Select a valid date.");
                    } else if (time < startDate) {
                        InvalidDateMessage("Selected date is before the start date. Select a valid date.");
                    } else {
                        endDate = time;
                        GetData();
                        endDateButton.Text = "End: " + endDate.ToShortDateString();
                    }
                });
                frag.Show(FragmentManager, DatePickerFragment.TAG);
            };

            // Settings click event
            settingsButton.Click += delegate {
                settingsButton.StartAnimation(spin);
                StartActivity(typeof(Settings));
            };

            //Gross sales field click event
            grossSalesField.Click += delegate
            {
                var intent = new Intent(this, typeof(SalesCatagories));
                intent.PutExtra("accessToken", accessToken);
                intent.PutExtra("siteId", siteId);
                intent.PutExtra("startDate", startDate.Ticks);
                intent.PutExtra("endDate", endDate.Ticks);
                StartActivity(intent);
            };
        }

        public async void GetData() {
            // Display loading spinner to notify user data is being refreshed
            progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Loading data...");
            progress.SetCancelable(false);
            RunOnUiThread(() => {
                progress.Show();
            });

            // Retrieve selected site information
            siteId = sharedPrefs.GetString("siteId", "");
            siteName = sharedPrefs.GetString("siteName", "");

            // Gather site data
            gather = new Gather(accessToken, siteId, startDate, endDate);
            await gather.GenerateTotalsAsync();

            grossSales.Text = gather.totalGrossSales.ToString("C");
            netSales.Text = gather.totalNetSales.ToString("C");
            payments.Text = gather.totalPayments.ToString("C");
            unfinishedTransactions.Text = gather.unfinishedTransactions.ToString("C");
            comps.Text = gather.totalComps.ToString("C");
            discounts.Text = gather.totalDiscounts.ToString("C");
            voids.Text = gather.totalVoids.ToString("C");
            guestCount.Text = gather.totalGuestCount.ToString();
            laborHours.Text = gather.totalHours.ToString();
            laborWages.Text = gather.totalWages.ToString("C");

            progress.Dismiss();

            gather = null;

            salesToolbar.Title = siteName + ": " + startDate.ToString("MM/dd") + "-" + endDate.ToString("MM/dd");
        }

        // Displays message when user selects invalid date
        public void InvalidDateMessage(string message) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle("Invalid Date");
            alert.SetMessage(message);
            alert.SetPositiveButton("OK", (senderAlert, args) => { });
            Dialog dialog = alert.Create();
            dialog.Show();
        }

        // Trims the memory usage of running applications
        public override void OnLowMemory() {
            Toast.MakeText(this, "Low memory", ToastLength.Short).Show();
            base.OnLowMemory();
        }
    }
}