﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using NorthStar.OrderEntryManager.Android.Authentication;

namespace NorthStar.OrderEntryManager.Android
{
    [Activity(Label = "SiteSelector")]
    public class SiteSelector : Activity
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;
        AllowedSite[] sites;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();

            // Set the selected theme
            switch (sharedPrefs.GetString("theme", "light"))
            {
                case "light":
                    SetTheme(Resource.Style.Theme_Light);
                    break;
                case "dark":
                    SetTheme(Resource.Style.Theme_Dark);
                    break;
            }
            SetContentView(Resource.Layout.SiteSelector);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.sites);

            // Get all sites under the account
            using (var client = new AuthenticationService())
            {
                string user = sharedPrefs.GetString("username", "");
                string pw = sharedPrefs.GetString("password", "");
                string appId = sharedPrefs.GetString("appId", "");

                var account = client.AuthenticateUser(user, pw, appId);
                sites = account.AllowedSites;
            }

            // Initialize LayoutManager, and Adapter
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(mLayoutManager);
            SiteAdapter siteAdapter = new SiteAdapter(sites);
            siteAdapter.ItemClick += OnItemClick;
            recyclerView.SetAdapter(siteAdapter);
            recyclerView.NestedScrollingEnabled = false;

            // What happens when you click on a site
            void OnItemClick(object sender, int position)
            {
                // Saves name and id of the clicked site
                sharedPrefsEditor.PutString("siteName", sites[position].SiteName).Apply();
                sharedPrefsEditor.PutString("siteId", sites[position].SiteId).Apply();

                // Clear the current activity
                // Switch back to the sales summary page and display new sales data
                Finish();
                var intent = new Intent(this, typeof(SalesSummary));
                intent.AddFlags(ActivityFlags.ClearTask);
                intent.AddFlags(ActivityFlags.NewTask);
                StartActivity(intent);
            }
        }
    }
}