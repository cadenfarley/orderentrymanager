﻿using System;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Support.V7.Widget;
using NorthStar.OrderEntryManager.Android.Authentication;

namespace NorthStar.OrderEntryManager.Android
{
    class SiteAdapter : RecyclerView.Adapter
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor sharedPrefsEditor;
        AllowedSite[] sites;

        public event EventHandler<int> ItemClick;

        public SiteAdapter(AllowedSite[] siteList) {
            sharedPrefs = Application.Context.GetSharedPreferences("prefs", FileCreationMode.Private);
            sharedPrefsEditor = sharedPrefs.Edit();
            sites = siteList;
        }

        public override int ItemCount {
            get { return sites.Length; }
        }

        void OnClick(int position) {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SiteViewHolder viewHolder = holder as SiteViewHolder;
            viewHolder.siteNameView.Text = sites[position].SiteName;

            if (position >= ItemCount-1) {
                viewHolder.siteSeparator.SetPadding(0, 0, 0, 0);
            } else {
                viewHolder.siteSeparator.SetPadding(0, 0, 0, 3);
            }
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            View rootView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Site, null, false);
            RecyclerView.LayoutParams layParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MatchParent,
                                                                                ViewGroup.LayoutParams.WrapContent);
            rootView.LayoutParameters = layParams;
            return new SiteViewHolder(rootView, OnClick);
        }
    }
}