﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using NorthStar.OrderEntryManager.DataService;

namespace NorthStar.OrderEntryManager.Test
{
    [TestClass]
    public class DataServiceTest
    {
        [TestMethod]
        public async Task DayTotalsAreCorrect() {
            Gather day = new Gather(new DateTime(2017, 06, 01), new DateTime(2017, 06, 01));

            Assert.IsTrue(day.totalGrossSales == 0);
            Assert.IsTrue(day.totalNetSales == 0);
            Assert.IsTrue(day.totalPayments == 0);
            Assert.IsTrue(day.totalComps == 0);
            Assert.IsTrue(day.totalDiscounts == 0);
            Assert.IsTrue(day.totalVoids == 0);
            Assert.IsTrue(day.totalGuestCount == 0);
            Assert.IsTrue(day.totalHours == 0);
            Assert.IsTrue(day.totalWages == 0);
            Assert.IsTrue(day.unfinishedTransactions == 0);

            await day.GenerateTotalsAsync();

            Assert.AreEqual((decimal?)219.63, day.totalGrossSales);
            Assert.AreEqual((decimal?)219.63, day.totalNetSales);
            Assert.AreEqual((decimal?)239.96, day.totalPayments);
            Assert.AreEqual((decimal?)0.00, day.totalComps);
            Assert.AreEqual((decimal?)0.00, day.totalDiscounts);
            Assert.AreEqual((decimal?)2.75, day.totalVoids);
            Assert.AreEqual(8, day.totalGuestCount);
            Assert.AreEqual((decimal?)0.00, day.totalHours);
            Assert.AreEqual((decimal?)0.00, day.totalWages);
            Assert.AreEqual((decimal?)-20.33, day.unfinishedTransactions);
        }

        [TestMethod]
        public async Task WeekTotalsAreCorrect() {
            Gather week = new Gather(new DateTime(2017, 06, 01), new DateTime(2017, 06, 07));

            Assert.IsTrue(week.totalGrossSales == 0);
            Assert.IsTrue(week.totalNetSales == 0);
            Assert.IsTrue(week.totalPayments == 0);
            Assert.IsTrue(week.totalComps == 0);
            Assert.IsTrue(week.totalDiscounts == 0);
            Assert.IsTrue(week.totalVoids == 0);
            Assert.IsTrue(week.totalGuestCount == 0);
            Assert.IsTrue(week.totalHours == 0);
            Assert.IsTrue(week.totalWages == 0);
            Assert.IsTrue(week.unfinishedTransactions == 0);

            await week.GenerateTotalsAsync();

            Assert.AreEqual((decimal?)1931.17, week.totalGrossSales);
            Assert.AreEqual((decimal?)1919.17, week.totalNetSales);
            Assert.AreEqual((decimal?)1370.35, week.totalPayments);
            Assert.AreEqual((decimal?)0.00, week.totalComps);
            Assert.AreEqual((decimal?)12.00, week.totalDiscounts);
            Assert.AreEqual((decimal?)52.99, week.totalVoids);
            Assert.AreEqual(135, week.totalGuestCount);
            Assert.AreEqual((decimal?)11.71, week.totalHours);
            Assert.AreEqual((decimal?)108.47, week.totalWages);
            Assert.AreEqual((decimal?)548.82, week.unfinishedTransactions);
        }

        [TestMethod]
        public async Task MonthTotalsAreCorrect() {
            Gather month = new Gather(new DateTime(2017, 06, 01), new DateTime(2017, 06, 30));

            Assert.IsTrue(month.totalGrossSales == 0);
            Assert.IsTrue(month.totalNetSales == 0);
            Assert.IsTrue(month.totalPayments == 0);
            Assert.IsTrue(month.totalComps == 0);
            Assert.IsTrue(month.totalDiscounts == 0);
            Assert.IsTrue(month.totalVoids == 0);
            Assert.IsTrue(month.totalGuestCount == 0);
            Assert.IsTrue(month.totalHours == 0);
            Assert.IsTrue(month.totalWages == 0);
            Assert.IsTrue(month.unfinishedTransactions == 0);

            await month.GenerateTotalsAsync();

            Assert.AreEqual((decimal?)31971.85, month.totalGrossSales);
            Assert.AreEqual((decimal?)30990.35, month.totalNetSales);
            Assert.AreEqual((decimal?)26477.15, month.totalPayments);
            Assert.AreEqual((decimal?)942.00, month.totalComps);
            Assert.AreEqual((decimal?)39.50, month.totalDiscounts);
            Assert.AreEqual((decimal?)603.49, month.totalVoids);
            Assert.AreEqual(635, month.totalGuestCount);
            Assert.AreEqual((decimal?)420.81, month.totalHours);
            Assert.AreEqual((decimal?)13560.37, month.totalWages);
            Assert.AreEqual((decimal?)4513.20, month.unfinishedTransactions);
        }
    }
}
